DeadNotes

A (currently very) simple deadline app taking its first steps. Main purposes of this app are:
- Add and delete deadlines
- Sync the deadlines between devices

Technologies used:
- Google Cloud SQL used to store the deadlines
- Google App Engine together with Flask (Python) to implement the API
- Linux client written in Qt (PySide2) and Python3
- Android client coming some day

I've tried my best not to reveal any sensitive data in this public repo, but if you happen to spot any, please be kind and tell me. :)
