from flask import Flask, jsonify, url_for, request, abort, make_response
from flask_httpauth import HTTPBasicAuth
import sqlalchemy
import decimal, datetime
import json

app = Flask(__name__)
app.config["DEBUG"] = True
auth = HTTPBasicAuth()


with open("qwidgettest.conf", "r") as qwtconf:
    """ Read configuration data """
    db = sqlalchemy.create_engine(qwtconf.readline())
    apiusr = qwtconf.readline()
    apipass = qwtconf.readline()


def alchemyencoder(obj):
    """JSON encoder function for SQLAlchemy special classes."""
    if isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return float(obj)


@auth.get_password
def get_password(username):
    """ Get password """
    if username == apiusr:
        return apipass
    return None


@auth.error_handler
def unauthorized():
    """ Unauthorized access error handler """
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.errorhandler(404)
def not_found(error):
    """ Not found error handler """
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route("/")
@auth.login_required
def index():
    """ INDEX """
    return "<h2>Test API for deadline app</h2>"


@app.route("/api")
@auth.login_required
def apiinfo():
    """ INFO about apis """
    return "<h2>Test API for deadline app</h2><p>GET: return all deadlines<p><br><p>POST: insert new deadline<p><br><p>DELETE: delete one deadline<p><br>"


@app.route("/api/deadlines", methods=["GET"])
@auth.login_required
def getdeadlines():
    """ READ all deadlines """
    with db.connect() as conn:
        try:
            sql = "SELECT * FROM deadlines"
            result = conn.execute(sql).fetchall()
        except:
            abort(400)
    return json.dumps([dict(r) for r in result], default=alchemyencoder)


@app.route("/api/deadlines", methods=["POST"])
@auth.login_required
def postdeadline():
    """ CREATE a new deadline """
    if not request.json or not 'title' in request.json:
        abort(400)
    with db.connect() as conn:
        try:
            sql = 'INSERT INTO deadlines (title, courseName, deadline) VALUES ("{}","{}","{}")'.format(request.json["title"], request.json["courseName"], request.json["deadline"])
            conn.execute(sql)
        except:
            abort(400)
    return jsonify({'result': True})


@app.route("/api/deadlines/<int:entryID>", methods=["DELETE"])
@auth.login_required
def deletedeadline(entryID):
    """ DELETE an existing deadline """
    with db.connect() as conn:
        try:
            sql = 'DELETE FROM deadlines WHERE entryID="{}"'.format(entryID)
            conn.execute(sql)
        except:
            return "except"
    return jsonify({'result': True})


if __name__ == "__main__":
  app.run()
