#!/usr/bin/env python3
"""
@Author Juho Holmi
"""

import sys
import json
import time
import random
import decimal
import datetime
import threading
from PySide2.QtCore import Qt, QSize, QDateTime, QTimer
from PySide2.QtGui import QIcon, QPixmap
from PySide2.QtWidgets import (QAction, QApplication, QHBoxLayout, QLabel, QMainWindow, QPushButton, QVBoxLayout, QWidget, QScrollArea, QWidget, QSizePolicy,
                                  QComboBox, QTabWidget, QStackedWidget, QFormLayout, QLineEdit, QCalendarWidget, QDateTimeEdit, QMenu, QMessageBox, QCheckBox)

#from flask import jsonify
#import sqlalchemy
#import requests

"""

with open("qwidgettest.conf", "r") as qwtconf:
    dburl = qwtconf.readline()
    apiusr = qwtconf.readline()
    apipass = qwtconf.readline()
    baseurl = qwtconf.readline()

db = sqlalchemy.create_engine(dburl)

def alchemyencoder(obj):
    #JSON encoder function for SQLAlchemy special classes.
    if isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return float(obj)

def fetchdeadlines():
    try:
        with db.connect() as conn:
            sql = "SELECT * FROM deadlines"
            result = conn.execute(sql).fetchall()
    except:
        return False
    return json.dumps([dict(r) for r in result], default=alchemyencoder)

def putdeadline(title, courseName, dl):
    postparams = {"title":title, "courseName":courseName, "dl":dl}
    try:
        requests.post(url = baseurl, json = postparams, auth=requests.auth.HTTPBasicAuth(apiusr,apipass))
    except:
        return False
    return True
"""

class MainWindow(QMainWindow):
    """ 
    Class for main window 
    """
    def __init__(self,widget):
        QMainWindow.__init__(self)
        self.setWindowTitle("DeadNotes - Home")
        self.setCentralWidget(widget)
        self.resize(0, 700) # resize to minimum horizontal size

class BottomWidget(QWidget):
    """ 
    Class for bottom widget containing sort by -dropdown menu, sync button and add deadline -button
    """
    def __init__(self):
        QWidget.__init__(self)
        self.setLayout(BottomHBox())

class ScrollableContentVBox(QVBoxLayout):
    """ 
    The main content layout containing the deadline objects 
    """
    def __init__(self):
        QVBoxLayout.__init__(self)
        self.layout().setAlignment(Qt.AlignTop)
        self.setContentsMargins(5,5,5,5)

        # GENERATE MOCKUP DATA
        importance_list  = ["Low", "Medium", "High"]
        for i in range(6): # Add labels to the scrollvbox layout
            title = "Testititteli {}".format(i+1)
            course = "Testikurssi {}".format(i+1)
            dl = QDateTime.currentDateTime()
            importance = random.choice(importance_list)

            dl = deadlineObject(title,course,dl,importance)
            self.addWidget(dl) 
        """

        deadlines = fetchdeadlines()
        deadlines = json.loads(deadlines)
        for d in deadlines:
            label = QLabel("<h3>{} ID: {}</h3>Course: {}<br>Deadline: {}".format(d["title"],d["entryID"],d["courseName"],d["dl"]))
        
            label.setContentsMargins(5,5,5,5)
            label.setStyleSheet("QLabel { background-color : grey; color : black; }")
            label.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
            label.setMinimumWidth(300)
            if i == 0:
                label.setStyleSheet("QLabel { background-color : white; color : black; }")
            if i == 1:
                label.setStyleSheet("QLabel { background-color : gray; color : black; }")
            self.addWidget(label)
            i = (i + 1) % 2
        """
        
class BottomHBox(QHBoxLayout):
    """ 
    Class for bottom widget layout containing sort by -dropdown menu, sync button and add deadline -button 
    """
    def __init__(self):
        QHBoxLayout.__init__(self)
        self.setSpacing(1)
        self.setMargin(1)

        self.sort_lbl = QLabel("Sort by: ")
        self.addWidget(self.sort_lbl)

        self.box = QComboBox()
        self.box.addItem("Closest")
        self.box.addItem("Farthest")
        self.box.addItem("Course name")
        self.box.addItem("Importance")
        self.box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        self.addWidget(self.box)

        self.sync_btn = QPushButton(QIcon("icons/syncicon.png"), "Sync")
        self.sync_btn.setEnabled(False)
        self.addWidget(self.sync_btn)

        self.add_btn = QPushButton(QIcon("icons/addicon.png"), "Add")
        self.add_btn.clicked.connect(self.add_dl)
        self.addWidget(self.add_btn)
    
    def add_dl(self):
        home_widget.add_deadline()

class centralTabWidget(QTabWidget):
    """ 
    Class that holds all other widgets 
    """
    def __init__(self):
        QTabWidget.__init__(self)
        self.setTabPosition(QTabWidget.South)
        self.currentChanged.connect(self.init_cwt_thread) # Init thread that changes the main window title based on tab we are looking

    def init_cwt_thread(self):
        """ Init thread that changes the main window title based on tab we are looking """
        title_thread = threading.Thread(target=self.change_window_title)
        title_thread.start()

    def change_window_title(self):
        """ Change main window title """
        try:
            time.sleep(0.01) # Sleep because tab index change has a delay
            i = self.currentIndex()
            if i == 0:
                window.setWindowTitle("DeadNotes - Home")
            if i == 1:
                window.setWindowTitle("DeadNotes - Settings")
            if i == 2:
                window.setWindowTitle("DeadNotes - Archive")
            if i == 3:
                window.setWindowTitle("DeadNotes - Help")
        except NameError as ne:
            pass # Ignore window not defined

class settingsWidget(QWidget):
    """ 
    Class for settings tab, currently just mockup tab without real functionality 
    """
    def __init__(self):
        QWidget.__init__(self)
        self.setLayout(QFormLayout())
        self.layout().setAlignment(Qt.AlignTop)

        title_lbl = QLabel("<h1>Settings</h1>")
        self.layout().addWidget(title_lbl)

        sync_lbl = QLabel("<h3>Set up sync</h3>Set up sync to sync data between devices.")
        sync_lbl.setWordWrap(True)
        self.layout().addWidget(sync_lbl)        

        delete_lbl = QLabel("<h3>Set up default delete behaviour</h3>Set up what happens by default when you delete a dl.")
        delete_lbl.setWordWrap(True)
        self.layout().addWidget(delete_lbl)       

        archive_lbl = QLabel("<h3>Set up archiving</h3>Set up how long deleted or expired deadlines are archived by default.")
        archive_lbl.setWordWrap(True)
        self.layout().addWidget(archive_lbl)        

class deadlineObject(QWidget):
    """ 
    Class for deadline objects 
    """
    def __init__(self,title,course,dl,importance):
        QWidget.__init__(self)
        self.setStyleSheet("background-color: white")
        self.title = title
        self.course = course
        self.dl = dl
        self.importance = importance
        self.done = False               
        self.setLayout(QHBoxLayout())

        if not self.done:
            self.title_lbl = QLabel("<h3>{}</h3>{}<h4>{}</h4>".format(title, course, dl.toString("dd.MM.yyyy hh:mm")))
        else:
            self.title_lbl = QLabel("<h3>{}</h3>{}<h4>{}</h4>".format(title, course, dl.toString("dd.MM.yyyy hh:mm")))
        self.title_lbl.setWordWrap(True)
        self.title_lbl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.title_lbl.setContentsMargins(10,0,0,0)

        ### Set border color according to importance
        if importance == "Low" and not self.done:
            self.title_lbl.setStyleSheet("border: 2px solid green;")
        elif importance == "Medium" and not self.done:
            self.title_lbl.setStyleSheet("border: 2px solid orange;")
        elif importance == "High" and not self.done:
            self.title_lbl.setStyleSheet("border: 2px solid red;")
        else:
            self.title_lbl.setStyleSheet("border: 2px dashed blue;")
        self.layout().addWidget(self.title_lbl)
        
        self.menu_btn = QPushButton()
        self.menu_btn.setIcon(QIcon("icons/threedoticon.png"))
        self.menu_btn.setStyleSheet('QPushButton::menu-indicator { image: none; };') # disable the down arrow, because we already have the three dots
        self.layout().addWidget(self.menu_btn)

        self.menu = QMenu()
        self.menu_btn.setMenu(self.menu)

        self.modify_actn = QAction("Modify")
        self.modify_actn.triggered.connect(self.modify)
        self.menu.addAction(self.modify_actn)

        self.mark_as_done_actn = QAction("Mark as done")
        self.mark_as_done_actn.triggered.connect(self.mark_as_done)
        self.menu.addAction(self.mark_as_done_actn)

        self.rm_actn = QAction("Remove")
        self.rm_actn.triggered.connect(self.delete)
        self.menu.addAction(self.rm_actn)
 
    def delete(self):
        """ Function for deleting the deadline """
        self.hide()
        self.deleteLater()
        self.parent().layout().removeWidget(self)
        msg_box = timedMessageBox(2000,"<p align='center'>Deadline deleted</p>")
        icon = QPixmap("icons/deleteicon.png")
        icon = icon.scaled(24,24)
        msg_box.setIconPixmap(icon)
        msg_box.show()
        timer = QTimer()
        timer.setInterval(2000)
        timer.setSingleShot(True)
        timer.timeout.connect(msg_box.hide)
        timer.start()

    def modify(self):
        """ Function for modifying the deadline. Only passes itself as a parameter to the real modifying function. """
        home_widget.modify_deadline(self)

    def mark_as_done(self):
        """ Function for marking the deadline as done """
        self.title_lbl.setStyleSheet("border: 2px dotted blue;")
        msg_box = timedMessageBox(2000,"<p align='center'>Deadline marked as done</p>")
        icon = QPixmap("icons/completedicon.png")
        icon = icon.scaled(24,24)
        msg_box.setIconPixmap(icon)
        msg_box.show()
        timer = QTimer()
        timer.setInterval(2000)
        timer.setSingleShot(True)
        timer.timeout.connect(msg_box.hide)
        timer.start()

class archiveWidget(QWidget):
    """ 
    Class for archive tab 
    """
    def __init__(self):
        QWidget.__init__(self)
        self.setLayout(QVBoxLayout())
        self.layout().setAlignment(Qt.AlignTop)

        title_lbl = QLabel("<h1>Archive</h1>")
        self.layout().addWidget(title_lbl)

        info_lbl = QLabel("Removed and completed deadlines get moved here.")
        info_lbl.setWordWrap(True)
        self.layout().addWidget(info_lbl) 

class helpWidget(QWidget):
    """ 
    Class for help tab and first-time info (look for if type == firsttime) 
    """
    def __init__(self,type=""):
        QWidget.__init__(self)
        self.setLayout(QVBoxLayout())
        self.layout().setAlignment(Qt.AlignTop)

        if type == "firsttime":
            title_lbl = QLabel("<h1>Hello!</h1>")
            self.layout().addWidget(title_lbl)

            info_lbl = QLabel("<h3>It seems that this is your first time using this app (or you haven't disabled this info)." 
                                "</h3>DeadNotes is an app designed to make your life easier.")            
        else:
            title_lbl = QLabel("<h1>Help</h1>")
            self.layout().addWidget(title_lbl)
            info_lbl = QLabel("<h3>Info about the app</h3>DeadNotes is an app designed to make your life easier.")

        info_lbl.setWordWrap(True)
        info_lbl.setContentsMargins(5,5,5,5)
        self.layout().addWidget(info_lbl)        

        help_lbl = QLabel("<h3>How to use</h3>- Press + Add to add new dl<br>- Press @ Sync to sync data between devices")
        help_lbl.setWordWrap(True)
        help_lbl.setContentsMargins(5,5,5,5)
        help_lbl.setStyleSheet("border-top: 1px solid grey;")
        self.layout().addWidget(help_lbl)   

        about_dls_label = QLabel("<h3>About deadlines</h3>"
                                    "<h4 >Colors signify importance</h4>"
                                    "<p style='color: green'>Green = low</p>"
                                    "<p style='color: orange'>Orange = medium</p>"
                                    "<p style='color: red'>Red = High</p>"
                                    "<p style='color: blue'>Dotted blue = Done</p>")
        about_dls_label.setWordWrap(True)
        about_dls_label.setContentsMargins(5,5,5,5)
        about_dls_label.setStyleSheet("border-top: 1px solid grey;")
        self.layout().addWidget(about_dls_label)   
        self.layout().addStretch(1)

        if type == "firsttime":
            self.disable_this_check = QCheckBox("Disable this info next time")
            self.layout().addWidget(self.disable_this_check)   

            ok_btn = QPushButton("OK, understood")
            ok_btn.clicked.connect(self.close)
            self.layout().addWidget(ok_btn)   

        version_lbl = QLabel("<h3>Version info</h3>DeadNotes beta version 0.1. This version is not for production use. Use it with care.")
        version_lbl.setWordWrap(True)
        version_lbl.setStyleSheet("border-top: 1px solid grey;")
        self.layout().addWidget(version_lbl)   

    def close(self):
        """ Function for closing the first-time info and saving the checkbutton state to config """
        if self.disable_this_check.isChecked():
            config = {"firsttime":"False"}
        else:
            config = {"firsttime":"True"}

        with open("config.json", "w") as f:
            json.dump(config, f)            
        home_widget.back_to_dl_list()

class addDeadlineWidget(QWidget):
    """ 
    Class for the tab that contains functionality to add a new deadline 
    """
    def __init__(self):
        QWidget.__init__(self)
        formLayout = QFormLayout()
        label = QLabel("<h1>Add deadline</h1><br>")
        label.setAlignment(Qt.AlignCenter)
        formLayout.addRow(label)
        self.title_edit = QLineEdit()
        self.title_edit.setPlaceholderText("Set title. Required field.")
        formLayout.addRow(self.tr("&Title"), self.title_edit)
        self.course_edit = QLineEdit()
        self.course_edit.setPlaceholderText("Set course name. Not required.")
        formLayout.addRow(self.tr("&Course"), self.course_edit)
        self.dl_edit = QDateTimeEdit()
        self.dl_edit.setCalendarPopup(True)
        self.dl_edit.setDateTime(QDateTime().currentDateTime())
        formLayout.addRow(self.tr("&Deadline"), self.dl_edit)

        importance_box = QComboBox()
        importance_box.addItem("Low")
        importance_box.addItem("Medium")
        importance_box.addItem("High")
        formLayout.addRow(self.tr("&Importance:"), importance_box)

        widget = QWidget()
        widget.setLayout(QHBoxLayout())
        self.add_btn = QPushButton("Add")
        self.add_btn.clicked.connect(self.add)
        widget.layout().addWidget(self.add_btn)
        self.cancel_btn = QPushButton("Cancel")
        self.cancel_btn.clicked.connect(self.cancel)
        widget.layout().addWidget(self.cancel_btn)
        formLayout.addRow(widget)

        self.setLayout(formLayout)

    def add(self):
        """ Function for adding the deadline """
        if self.title_edit.text() == "":
            self.title_edit.setStyleSheet("border: 2px solid red")
            self.title_edit.setPlaceholderText("Sorry, this field is required.")
        else:
            dl = deadlineObject(self.title_edit.text(),self.course_edit.text(),self.dl_edit.dateTime(), "Low")
            scrollable_content_vbox.addWidget(dl)
            self.title_edit.clear()
            self.course_edit.clear()
            home_widget.back_to_dl_list("Success")        

    def cancel(self):
        """ Function for canceling the adding process """
        self.title_edit.clear()
        self.course_edit.clear()
        self.dl_edit.clear()
        home_widget.back_to_dl_list("Cancel")  

class mofidyDeadlineWidget(QWidget):
    """ 
    Class for the view that contains functionality to modify an existing deadline 
    """
    def __init__(self):
        QWidget.__init__(self)

        self.label = None

        formLayout = QFormLayout()
        label = QLabel("<h1>Modify deadline</h1><br>")
        label.setAlignment(Qt.AlignCenter)
        formLayout.addRow(label)
        self.title_edit = QLineEdit()
        formLayout.addRow(self.tr("&Title"), self.title_edit)
        self.course_edit = QLineEdit()
        formLayout.addRow(self.tr("&Course"), self.course_edit)
        self.dl_edit = QDateTimeEdit()
        self.dl_edit.setCalendarPopup(True)
        formLayout.addRow(self.tr("&Deadline"), self.dl_edit)

        box = QComboBox()
        box.addItem("Low")
        box.addItem("Medium")
        box.addItem("High")
        formLayout.addRow(self.tr("&Importance"), box)

        widget = QWidget()
        widget.setLayout(QHBoxLayout())

        self.modify_btn = QPushButton("Modify")
        self.modify_btn.clicked.connect(self.modify_label)
        widget.layout().addWidget(self.modify_btn)

        self.rm_btn = QPushButton("Remove")
        self.rm_btn.clicked.connect(self.remove_label)
        widget.layout().addWidget(self.rm_btn)

        self.cancel_btn = QPushButton("Cancel")
        self.cancel_btn.clicked.connect(self.cancel)
        widget.layout().addWidget(self.cancel_btn)

        formLayout.addRow(widget)

        self.setLayout(formLayout)

    def modify_label(self):
        """ Function for modifying the deadline """
        if self.title_edit.text() == "": # Don't let the user submit a deadline with empty title
            self.title_edit.setStyleSheet("border: 2px solid red")
            self.title_edit.setPlaceholderText("Sorry, this field is required.")
        else:
            self.label.title = self.title_edit.text()
            self.label.course = self.course_edit.text()
            self.label.dl = self.dl_edit.dateTime()
            self.label.title_lbl.setText("<h3>{}</h3>{}<br>{}".format(self.title_edit.text(), self.course_edit.text(), self.dl_edit.dateTime().toString("dd.MM.yyyy hh:mm")))
            home_widget.back_to_dl_list("Success")
    
    def cancel(self):
        """ Function for canceling the modifying process """
        home_widget.back_to_dl_list("Cancel")  

    def remove_label(self):
        """ Function for removing the deadline """
        home_widget.back_to_dl_list("Deleted")  
        self.label.hide()
        self.label.deleteLater()
        self.label.parent().layout().removeWidget(self)  
        self.label = None      

class timedMessageBox(QMessageBox):
    """ 
    Class for an auto-closing message box 
    """
    def __init__(self, timeout, message):
        super(timedMessageBox, self).__init__()
        self.timeout = timeout
        self.setParent(home_widget)
        self.setText(message)
        self.setStyleSheet("background-color: white;")
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setModal(False)

    def showEvent(self, event):
        QtCore.QTimer().singleShot(self.timeout, self.close)
        super(timedMessageBox, self).showEvent(event)

class homeWidget(QStackedWidget):
    """ 
    Class that contains home-related (view, add or modify deadlines) widgets  
    """
    def __init__(self):
        QStackedWidget.__init__(self)
    
    def back_to_dl_list(self,msg=""):
        """ Function for going back to the deadlines list view """
        self.setCurrentIndex(0)
        window.setWindowTitle("DeadNotes - Home")
        if msg != "":
            if msg == "Success":
                msg_box = timedMessageBox(2000,"<p align='center'>Success!</p>")
                icon = QPixmap("icons/completedicon.png")
            elif msg == "Cancel":
                msg_box = timedMessageBox(2000,"<p align='center'>Changes discarded</p>")
                icon = QPixmap("icons/cancelicon.png")
            elif msg == "Deleted":
                msg_box = timedMessageBox(2000,"<p align='center'>Deadline deleted</p>")
                icon = QPixmap("icons/deleteicon.png")
            icon = icon.scaled(24,24)
            msg_box.setIconPixmap(icon)
            msg_box.show()
            timer = QTimer()
            timer.setInterval(2000)
            timer.setSingleShot(True)
            timer.timeout.connect(msg_box.hide)
            timer.start()

    def modify_deadline(self, label):
        """ Function for changing to modify deadline -view. """
        self.setCurrentIndex(1)
        # Set the field values to same as label's we are modifying.
        self.currentWidget().title_edit.setText(label.title)
        self.currentWidget().course_edit.setText(label.course)
        self.currentWidget().dl_edit.setDateTime(label.dl)
        self.currentWidget().label = label
        window.setWindowTitle("DeadNotes - Modify deadline")

    def add_deadline(self):
        """ Function for changing to add deadline -view """
        self.widget(2).dl_edit.setDateTime(QDateTime().currentDateTime()) # Set the date field value to current date.
        self.setCurrentIndex(2)
        window.setWindowTitle("DeadNotes - Add deadline")

def check_if_not_first_time():
    """ 
    Helper function to check if user is using the app for the first time 
    """
    try:
        with open("config.json", "r") as f:
            config = json.load(f)

            if config["firsttime"] == "True":
                return False # First time
            else:
                return True # Not first time
    except:
        return False

def construct_app():
    """ 
    Function for initialising widgets and starting the app
    """
    global home_widget              # Make these three global
    global scrollable_content_vbox  # so it is possible 
    global window                   # to access them easily

    firsttime = False
    if not check_if_not_first_time():   # Check if first time using the app
        firsttime = True

    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon("icons/icon.png"))

    central_vbox = QVBoxLayout()

    deadlineswidget = QWidget()
    deadlineswidget.setLayout(central_vbox)
    add_dl_widget = addDeadlineWidget()
    modify_dl_widget = mofidyDeadlineWidget()

    home_widget = homeWidget() # Stacked widget which contains deadline list, add dl view and modify dl view

    home_widget.addWidget(deadlineswidget)
    home_widget.addWidget(add_dl_widget)
    home_widget.addWidget(modify_dl_widget)

    setting_widget = settingsWidget()   # These three are
    archive_widget = archiveWidget()    # just mockup tabs
    help_widget = helpWidget()          # with no functionality

    central_tab_widget = centralTabWidget() # The widget containing home, archive, settings and help tabs
    central_tab_widget.addTab(home_widget, QIcon("icons/homeicon.png"), "Home")
    central_tab_widget.addTab(setting_widget, QIcon("icons/settingsicon.png"), "Settings")
    central_tab_widget.addTab(archive_widget, QIcon("icons/archiveicon.png"),"Archive")
    central_tab_widget.addTab(help_widget, QIcon("icons/helpicon.png"),"Help")
    
    scrollable_content_vbox = ScrollableContentVBox() # The vertical box layout containing the deadline objects (labels)
    scroll_widget = QWidget()
    scroll_widget.setLayout(scrollable_content_vbox) # Set scroll_widget's layout to scrollvbox

    if firsttime:   # If first time using the app, add first-time info and show it first
        home_widget.addWidget(helpWidget("firsttime"))
        home_widget.setCurrentIndex(3)

    scroll_area = QScrollArea()
    scroll_area.setWidgetResizable(True)
    scroll_area.setWidget(scroll_widget) # Set scroll_area's widget to scrollWidget
    central_vbox.addWidget(scroll_area) # Add the scroll_area to the main widget's layout

    bottom_widget = BottomWidget()
    central_vbox.addWidget(bottom_widget)

    window = MainWindow(central_tab_widget) # Main window
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    construct_app()
    